export function debounce(f, timeout = 300) {
    let id = 0;
    return function (...args) {
        clearTimeout(id);
        id = setTimeout(function () {
            return f(...args);
        }, timeout);
    }
}


export function sortBy(list, key, isReverse) {
    const sortedList = list.slice().sort((item1, item2) => {
        if (typeof (item1[key]) === "number") {
            return (item1[key] || 0) - (item2[key] || 0)
        }
        if (typeof (item1[key]) === "string" && typeof (item2[key]) === "string") {
            if (item1[key].toLowerCase() < item2[key].toLowerCase()) {
                return -1
            } else if (item1[key].toLowerCase() > item2[key].toLowerCase()) {
                return +1;
            } else {
                return 0;
            }
        }
        return 1;
    })

    return isReverse ? sortedList.reverse() : sortedList
}


export function compose(g, f) {
    return function (x) {
        return g(f(x));
    }
}


export function equal(v1, v2) {

    if (v1 === v2) {
        return true;
    }

    if (typeof v1 !== typeof v2 || typeof v1 === "function" || typeof v2 === "function") {
        return false;
    }

    if (typeof v1 === "number" || typeof v1 === "string" || typeof v1 === "boolean") {
        return false;
    }

    if (Array.isArray(v1) && Array.isArray(v2)) {
        if (v1.length !== v2.length) {
            return false;
        }

        for (let i = 0; i < v1.length; i++) {
            if (!equal(v1[i], v2[i])) {
                return false;
            }
        }
        return true;
    }

    const k1 = Object.keys(v1);
    k1.sort();
    const k2 = Object.keys(v2);
    k2.sort();

    if (!equal(k1, k2)) {
        return false;
    }

    for (let k of k1) {
        if (!equal(v1[k], v2[k])) {
            return false;
        }
    }
    return true;
}

