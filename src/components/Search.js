import { debounce } from "../utils";
import InputWithLabel from "./InputWithLabel";


export default function Search({ searchTerm, setSearchTerm }) {


  function handleChange(event) {
    setSearchTerm(event.target.value);
  }

  return (
    <>
      <InputWithLabel
        id="search"
        defaultValue={searchTerm}
        isFocused={true}
        onInputChange={debounce(handleChange)}
      >
        <strong>Search:</strong>
      </InputWithLabel>

      <p>
        Searching for <strong>{searchTerm}</strong>.
      </p>
    </>
  );
}
