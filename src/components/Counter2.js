import React, { useEffect, useState, useRef } from 'react';

function Counter() {
    const [state, setState] = useState(1);

    const ref = useRef(state);

    useEffect(() => {
        setTimeout(() => {
            setState(state + 1)
            ref.current = ref.current + 1;
        }, 1000);
        setTimeout(function () {
            //console.log(">>>>>>>", state)
            //console.log("++++++", ref.current);

        }, 2000)
    }, [state]);

    return <>
        <h1>{state}</h1>
    </>;
}
export default Counter;