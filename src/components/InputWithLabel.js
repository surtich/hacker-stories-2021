import { useEffect, useRef } from "react";

const InputWithLabel = ({ id, type = "text", value, defaultValue, onInputChange, isFocused = false, children }) => {

    const inputRef = useRef();

    useEffect(() => {
        if (isFocused && inputRef.current) {
            inputRef.current.focus();
        }
    }, [isFocused]);

    return (<>
        <label htmlFor={id}>{children}</label>
        &nbsp;
        <input
            id={id}
            ref={inputRef}
            type={type}
            value={value}
            defaultValue={defaultValue}
            onChange={onInputChange}
        />
    </>);
}

export default InputWithLabel;