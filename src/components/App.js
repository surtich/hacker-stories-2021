import { useState, useEffect, useReducer, useRef, useMemo } from 'react';
import { Outlet, Link } from "react-router-dom";
import List from "./List";
import Search from "./Search";
import Example from "./Example";
import Counter from "./Counter2";
import useSemiPersistentState from "../useSemiPersistentState";
import { withLoading } from './withLoading';
import { withBackground } from './withBackground';
import { compose } from '../utils';
import { ActionsContext } from '../context';
import { useCallback } from 'react';

const API_ENDPOINT = 'https://hn.algolia.com/api/v1/search?query=';

const initialStoriesState = {
  loading: false,
  error: false,
  stories: []
}
const storiesReducer = (state, action) => {
  switch (action.type) {
    case "STORIES_FETCH_INIT":
      return {
        ...state,
        loading: true,
        error: false
      }
    case "STORIES_CLEAN":
      return {
        ...state,
        stories: []
      }
    case "STORIES_FETCH_SUCCESS":
      return {
        loading: false,
        error: false,
        stories: [...state.stories, ...action.payload]
      }
    case "STORIES_FETCH_FAILURE":
      return {
        loading: false,
        error: true,
        stories: []
      }
    case "STORIES_DELETE":
      return {
        ...state,
        stories: state.stories.filter(story => story.objectID !== action.payload)
      }
    default:
      throw Error("Action Stories Error!")
  }
};

function App() {

  const [search, setSearch] = useSemiPersistentState("search", { term: "React", page: 0, pages: 0 });
  const lastSearchVersion = useRef(0);
  const [searchVersion, setSearchVersion] = useState(1);
  const [storiesState, dispatchStories] = useReducer(
    storiesReducer,
    initialStoriesState
  );

  useEffect(() => {
    loadStories(search);
    setSearchVersion(searchVersion => searchVersion + 1);
    lastSearchVersion.current = lastSearchVersion.current + 1;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [search.term, search.page]);

  useEffect(() => {
    dispatchStories({ type: 'STORIES_CLEAN' });
  }, [search.term]);

  function setSearchTerm(searchTerm) {
    setSearch({ term: searchTerm, page: 0, pages: 0, });
  }

  function showMore() {
    setSearch({ ...search, page: search.page + 1 });
  }

  async function loadStories(search) {
    dispatchStories({ type: 'STORIES_FETCH_INIT' });
    try {
      const response = await fetch(`${API_ENDPOINT}${search.term}&page=${search.page}`);
      const result = await response.json();
      if (searchVersion === lastSearchVersion.current) {
        dispatchStories({
          type: 'STORIES_FETCH_SUCCESS',
          payload: result.hits,
        });
        setSearch({ ...search, pages: result.nbPages });
      }
    } catch (error) {
      dispatchStories({ type: 'STORIES_FETCH_FAILURE' });
    }
  }

  const onDelete = useCallback((objectID) => {
    dispatchStories({
      type: "STORIES_DELETE",
      payload: objectID
    });
  }, [dispatchStories]);

  const valueActionsContext = useMemo(() => ({
    onDelete
  }), [onDelete]);

  const loadingNew = storiesState.loading && search.page === 0;
  const loadingMore = storiesState.loading && search.page !== 0;

  return (
    <div>
      <h1>My Hacker Stories</h1>
      <Example />
      <Counter />
      <Search searchTerm={search.term} setSearchTerm={setSearchTerm} />
      <hr />
      <ActionsContext.Provider value={valueActionsContext}>
        <List searchTerm={search.term} {...storiesState} loading={loadingNew} loadStories={loadStories} />
      </ActionsContext.Provider>
      <ShowMore style={{ textTransform: "uppercase" }} showMore={showMore} disabled={search.page >= search.pages} />
      <ShowMoreWithLoading style={{ textTransform: "uppercase" }} loading={loadingMore} showMore={showMore} disabled={search.page >= search.pages} />
      <nav
        style={{
          borderBottom: "solid 1px",
          paddingBottom: "1rem"
        }}
      >
        <Link to="/invoices">Invoices</Link> |{" "}
        <Link to="/expenses">Expenses</Link>
      </nav>
      <Outlet />
    </div>
  )
}

function ShowMore({ showMore, disabled, style = {} }) {
  return <button onClick={showMore} disabled={disabled} style={style}>Show More</button>
}

const ShowMoreWithLoading = compose(withBackground("green"), withLoading)(ShowMore);


export default App;
