import React from 'react';
import { useContext } from 'react/cjs/react.development';
import styled from 'styled-components';
import { ActionsContext } from '../context';
import { equal } from '../utils';
import "./Item.scss";

const pointsStyle = {
  color: "red",
  fontSize: "20px"
};

const Comments = styled.span`
  font-size: 1.5em;
  text-align: center;
  color: palevioletred;
`;

function Item({ objectID, url, title, author, num_comments, points, style = {}, buttonStyle = {} }) {
  const { onDelete } = useContext(ActionsContext)
  return (
    <div style={style}>
      <span style={{ width: '40%' }}>
        <a href={url}>{title}</a>
      </span>
      <span className="item-author" style={{ width: '30%' }}>{author}</span>
      <Comments style={{ width: '10%' }} >{num_comments}</Comments>
      <span style={{ ...pointsStyle, width: '10%' }}>{points}</span>
      <span style={{ width: '10%' }}>
        <button style={buttonStyle} onClick={() => onDelete(objectID)}>delete</button>
      </span>
    </div>
  )
}


export default React.memo(Item, equal);