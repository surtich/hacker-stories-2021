export function withBackground(color = "red") {
    return function (WrappedComponent) {
        return ({ style = {}, ...props }) => {
            return <WrappedComponent {...props} style={{ ...style, backgroundColor: color }} />
        }
    }
}