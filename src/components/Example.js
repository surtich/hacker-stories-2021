import React, { useState, useEffect } from 'react';

function Example() {
    const [windowWidthSize, setWindowWidthSize] = useState(0);
    const [show, setShow] = useState(true);

    useEffect(() => {
        function handleResize(e) {

            //console.log("resize")
            const { width } = document.body.getBoundingClientRect();

            setWindowWidthSize(Math.ceil(width));
        }

        //console.log("effect")

        if (show) {
            handleResize();
            window.addEventListener('resize', handleResize)
        }

        return () => {
            //console.log("clean effect")
            window.removeEventListener('resize', handleResize)
        }
    }, [show]);


    //console.log("render")

    return (
        <h1>
            {show && "The window size" + windowWidthSize + " pixels"}
            <button onClick={() => setShow(!show)}>{show ? "Hide" : "Show"}</button>
        </h1>
    )
}

export default Example;