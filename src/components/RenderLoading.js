export function Loading({ loading, children }) {
    return loading ? <p>Loading...</p> : children()
}

