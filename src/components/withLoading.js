export function withLoading(WrappedComponent, LoadingComponent = () => <p>Loading...</p>) {
    return ({ loading, ...props }) => {
        return loading ? <LoadingComponent /> : <WrappedComponent {...props} />
    }
}