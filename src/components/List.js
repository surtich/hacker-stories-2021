import { useState, useEffect } from "react";
import { sortBy } from "../utils";
import Item from "./Item";
import { withLoading } from "./withLoading";

const SORTS = {
  NONE: (list) => list,
  TITLE: (list, isReverse) => sortBy(list, 'title', isReverse),
  AUTHOR: (list, isReverse) => sortBy(list, 'author', isReverse),
  COMMENT: (list, isReverse) => sortBy(list, 'num_comments', isReverse).reverse(),
  POINT: (list, isReverse) => sortBy(list, 'points', isReverse).reverse(),
};

const colors = ["red", "green", "pink"];

function List({ searchTerm, stories, error, loadStories }) {

  const [sort, setSort] = useState({
    sortKey: 'NONE',
    isReverse: false,
  });

  const [lastStories, setLastStories] = useState([]);
  const [newStories, setNewStories] = useState([]);

  useEffect(() => {
    setNewStories(stories.filter(story => !lastStories.find(lstory => lstory.objectID === story.objectID)))
    setLastStories(stories);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [stories]);

  const sortFunction = SORTS[sort.sortKey];
  const sortedList = sortFunction(stories, sort.isReverse);

  const handleSort = (sortKey) => {
    setSort({
      sortKey,
      isReverse: sortKey === sort.sortKey ? !sort.isReverse : false
    })
  };

  function storyStyle(story) {
    const style = { display: "flex" }

    if (lastStories.length === newStories.length) {
      return style;

    }
    const isNewStory = newStories.find(nstory => story.objectID === nstory.objectID);

    if (isNewStory) {
      style.backgroundColor = "red";
    }

    return style;
  }

  if (error) {
    return <>
      <p>Error!</p>
      <button onClick={() => loadStories(searchTerm)}>Retry</button>
    </>
  }

  return <ul>
    <li style={{ display: 'flex' }}>
      <span style={{ width: '40%' }}>
        <button type="button" onClick={() => handleSort('TITLE')}>
          Title
        </button>
      </span>
      <span style={{ width: '30%' }}>
        <button type="button" onClick={() => handleSort('AUTHOR')}>
          Author
        </button>
      </span>
      <span style={{ width: '10%' }}>
        <button type="button" onClick={() => handleSort('COMMENT')}>
          Comments
        </button>
      </span>
      <span style={{ width: '10%' }}>
        <button type="button" onClick={() => handleSort('POINT')}>
          Points
        </button>
      </span>
      <span style={{ width: '10%' }}>Actions</span>
    </li>
    {sortedList.map(function (story, i) {
      return (
        <li key={story.objectID}>
          <Item {...story} buttonStyle={{ color: colors[i % colors.length] }} style={storyStyle(story)} />
        </li>
      );
    })}
  </ul>;
}

export default withLoading(List);