import { useState, useEffect } from "react";

const useSemiPersistentState = (key, initialState) => {
    const [value, setValue] = useState(
        JSON.parse(sessionStorage.getItem(key)) || initialState
    );

    useEffect(() => {
        sessionStorage.setItem(key, JSON.stringify(value));
    }, [value, key]);

    return [value, setValue];
};

export default useSemiPersistentState;